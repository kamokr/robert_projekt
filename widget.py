#!/usr/bin/env python3
import datetime
import logging
import os
import time
from optparse import OptionParser

import cherrypy
from PIL.ImageQt import ImageQt
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import (QApplication, QDesktopWidget, QFrame, QHBoxLayout,
                             QLabel, QLineEdit, QPushButton, QSizePolicy,
                             QStackedWidget, QVBoxLayout, QWidget)
import RPi.GPIO as gpio

from generator import Generator, code


STYLE = f"""
QWidget {{
    background-color: #909090;
}}"""

class State:
    WAIT = 1
    MEASURE = 2
    RESULT = 3
    END = 4


MAX_WAIT_TIME = 3


class MainWindow(QWidget):
    def __init__(self, tests, mode='normal'):
        QWidget.__init__(self)

        self.timer = QTimer()
        # self.timer.setSingleShot(True)
        self.timer.setInterval(MAX_WAIT_TIME*1000)

        self.timer.timeout.connect(self.timer_event)

        gpio.setup(40, gpio.IN, pull_up_down=gpio.PUD_UP)
        gpio.add_event_detect(40, gpio.FALLING, callback=self.input_event, bouncetime=100)

        self.g = Generator()
        self.setGeometry(0, 0, 1024, 600)
        self.label = QLabel(self)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.hide()
        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.state = State.WAIT
        self.tstart = 0
        self.plate = None

        self.nr_pomiaru = 1
        self.max_pomiar = tests
        self.mode = mode

        if self.max_pomiar > 0 and mode == 'normal':
            self.filename = f'/root/results/auto_{datetime.datetime.now().strftime("%Y.%m.%d_%H-%M-%S.%f.txt")}'
            with open(self.filename, 'w') as f:
                f.write('Nr pomiaru;Odczytany numer;Czas pomiaru;Sprawdzany numer;Odczyt poprawny\r\n')

            self.timer.start()

        elif mode == 'ext':
            self.nr_pomiaru = 0
            self.filename = f'/root/results/plener_{datetime.datetime.now().strftime("%Y.%m.%d_%H-%M-%S.%f.txt")}'
            with open(self.filename, 'w') as f:
                f.write('Nr pomiaru;Odczytany numer;Czas pomiaru;Sprawdzany numer;Odczyt poprawny\r\n')

    def input_event(self, i):
        self.nr_pomiaru += 1
        now = time.time()
        if now - self.tstart < MAX_WAIT_TIME:
            return
        self.tstart = now
        self.state = State.MEASURE
        print(f':: nr pomiaru: {self.nr_pomiaru}')

    def timer_event(self):
        if self.nr_pomiaru > self.max_pomiar:
            self.state == State.END
            return

        if self.state == State.WAIT:
            self.show_plate()
            self.state = State.MEASURE

        elif self.state == State.MEASURE:
            print(f'   brak odczytu')

            with open(self.filename, 'a') as f:
                f.write(f'{self.nr_pomiaru};brak odczytu;{MAX_WAIT_TIME*1000};{self.plate};nie\r\n')

            self.nr_pomiaru += 1
            self.hide_plate()
            self.state = State.WAIT

        elif self.state == State.RESULT:
            self.nr_pomiaru += 1
            self.hide_plate()
            self.state = State.WAIT

        # self.timer.start()
        
    def hide_plate(self):
        if self.label.isVisible():
            self.label.hide()
            # print(':: hiding plate')

    def show_plate(self):
        if not self.label.isVisible():
            self.update()
            self.label.show()
            self.tstart = time.time()
            print(f':: nr pomiaru: {self.nr_pomiaru} wyświetlane: {self.plate}')

    def update(self):
        plate = code()
        self.plate = plate[2].replace(' ', '').upper()
        plate_image, _ = self.g.render_plate(plate)
        self.label.setPixmap(QPixmap.fromImage(ImageQt(plate_image)))

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Space:
            if self.label.isVisible():
                self.label.hide()
            else:
                self.update()
                self.label.show()

    def on_plate_received(self, plate):
        if self.state != State.MEASURE:
            return

        self.state = State.RESULT

        total = time.time() - self.tstart
        correct = 'tak' if plate == self.plate else 'nie'
        print(f'   odebrano: {plate}, czas: {int(total*1000)} ms, poprawnie: {correct}')

        with open(self.filename, 'a') as f:
            f.write(f'{self.nr_pomiaru};{plate};{int(total*1000)};{self.plate};{"tak" if correct == "tak" else ""}\r\n')


class EventApi(object):
    exposed = True

    def __init__(self, callback):
        self.callback = callback

    def POST(self, **kwargs):
        # print(f':: received {kwargs["licensePlate"]}')
        self.callback(kwargs['licensePlate'])


class StaticIndex(object):
    @cherrypy.expose
    def index(self):
        fnames = []
        for (dirpath, dirnames, filenames) in os.walk('/root/results'):
            fnames.extend(filenames)
            break

        fnames.sort()
        
        html = ''
        for f in fnames:
            html += f'<a href="/wyniki/download/{f}">{f}</a> <a href="/wyniki/delete/{f}" onclick="window.location.reload(true);">USUŃ</a><br>'

        return html

    @cherrypy.expose
    def delete(self, *args):
        try:
            os.remove(f'/root/results/{args[0]}')
        except Exception as ex:
            logging.exception(ex)
        
        return self.index()


class App():
    def run(self, tests, mode):
        app = QApplication([])
        app.setStyleSheet(STYLE)
        mainwindow = MainWindow(tests=tests, mode=mode)
        mainwindow.show()

        cherrypy.config.update({'log.screen': False,
            'log.access_file': '',
            'log.error_file': '',
            'request.methods_with_bodies': ('POST', 'PUT', 'PATCH'),
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 12080,
            'server.protocol_version': 'HTTP/1.0',
            'engine.autoreload.on': False,
            

            # 'global': {
            #         'environment' : 'embedded',
            # }
        })

        cherrypy.tree.mount(
            EventApi(mainwindow.on_plate_received),
            '/event', 
            {
                '/': {
                    'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                },
            }
        )
        cherrypy.tree.mount(
            StaticIndex(),
            '/wyniki',
            {
                '/download': {
                    'tools.staticdir.root': '/root',
                    'tools.staticdir.on': True,
                    'tools.staticdir.dir': 'results',
                }
            }
        )
        cherrypy.server.socket_host = '0.0.0.0'
        cherrypy.server.socket_port = 12080
        cherrypy.engine.start()

        app.exec_()

        cherrypy.engine.exit()


if __name__ == '__main__':
    gpio.setmode(gpio.BOARD)

    parser = OptionParser()
    parser.add_option("-n", "--num_tests", action="store", type="int", dest="tests", default=10)
    parser.add_option("-m", "--mode", action="store", type="str", dest="mode", default='normal')
    (options, args) = parser.parse_args()

    if options.mode == 'normal':
        print(f':: test uruchomiony, liczba pomiarów: {options.tests}')
    elif options.mode == 'ext':
        print(f':: test uruchomiony, tryb plenerowy')
    else:
        parser.print_usage()
        sys.exit(0)

    App().run(tests=options.tests, mode=options.mode)
