#!/usr/bin/env python

import argparse
import glob
import os
import math  
import ntpath
import tkinter as tk
import random
from collections import namedtuple

import numpy as np
from PIL import Image, ImageDraw, ImageTk, ImageEnhance

import util
from generator import Generator, GeneratorConfig, code

WINDOW_WIDTH = 506


# override for custom generator parameters
class CustomGeneratorConfig(GeneratorConfig):
    # BACKGROUNDS_DIR = '../SUN397'
    # PLATE_HEIGHT = 114
    # PLATE_WIDTH = 520
    # TEXT_OFFSET_TOP = 17

    DATASET_WIDTH = 128
    # DATASET_HEIGHT = 64


    # randomize augmentation (probability, min value, max value)
    # RANDOM_SCALE = (1.0, 0.4, 0.9)
    # RANDOM_YAW = (1.0, -40, 40)
    # RANDOM_PITCH = (1.0, -15, 30)
    # RANDOM_ROLL = (1.0, -10, 10)
    # RANDOM_BRIGHTNESS = (1.0, 0.5, 1.5)
    # RANDOM_CONTRAST = (1.0, 0.5, 1.5)
    # RANDOM_SHARPNESS = (1.0, -0.5, 1.0)
    # RANDOM_NOISE = (1.0, 1.0, 10.0)

    # Image.transform doesn't support Lanczos filtering, so upscale for higher quality
    # UPSCALE = 2
    pass



class GeneratorTool(tk.Frame):
    def __init__(self, master):
        super().__init__(master)

        cfg = CustomGeneratorConfig()
        self.g = Generator(cfg)

        self.yaw = 0
        self.pitch = 0
        self.roll = 0
        self.distance = 0
        self.scale = 0.9

        self.image1 = None
        self.image1_mask = None

        self.image2_mask = None
        self.image2 = None

        self.canvas1 = tk.Canvas(self.master, width=WINDOW_WIDTH, height=100, background='black')
        self.canvas1.pack(fill='both', expand=True)
        self.canvas2 = tk.Canvas(self.master, width=WINDOW_WIDTH, height=64, background='black')
        self.canvas2.pack(fill='both', expand=True)

        self.master.bind('<Key>', self.key)

    def generate(self):
        #self.image1, self.image1_mask = self.g.render_plate(code())
        # self.image1, self.image1_mask = self.g.render_text(code())
        self.image2 = self.g.generate(
            self.image1, 
            self.image1_mask, 
            self.yaw, 
            self.pitch, 
            self.roll, 
            self.distance, 
            scale=0.9, 
            position=(0, 0))

    def random_plate(self):
        self.image1, self.image1_mask = self.g.render_plate(code())
        self.image2 = self.g.generate(
            self.image1, 
            self.image1_mask)

    def random_text(self):
        text = code()[2]
        self.image1, self.image1_mask = self.g.render_text(text, font='ttf/arklatrs.ttf')
        self.image2 = self.g.generate(
            self.image1, 
            self.image1_mask)

    def random_text2(self):
        self.image1, self.image1_mask = self.g.render_text()
        self.image2 = self.g.generate(
            self.image1, 
            self.image1_mask)

    def redraw(self):
        if self.image1:
            self.tkimage1 = ImageTk.PhotoImage(self.image1)
            self.canvas1.create_image(0, 0, anchor=tk.NW, image=self.tkimage1)

        if self.image2:
            self.tkimage2 = ImageTk.PhotoImage(self.image2)
            self.canvas2.create_image(WINDOW_WIDTH//2, self.image2.size[1]//2, anchor=tk.CENTER, image=self.tkimage2)

    def key(self, event):
        print('key:', event.keysym, 'state:', event.state)
        if event.keysym == 'space':
            pass

        if event.keysym == 'd':
            self.yaw += 5
            self.yaw = max(-90, min(90, self.yaw))

        if event.keysym == 'a':
            self.yaw -= 5
            self.yaw = max(-90, min(90, self.yaw))

        if event.keysym == 's':
            self.pitch += 5
            self.pitch = max(-90, min(90, self.pitch))

        if event.keysym == 'w':
            self.pitch -= 5
            self.pitch = max(-90, min(90, self.pitch))

        if event.keysym == 'e':
            self.roll += 1
            self.roll %= 360

        if event.keysym == 'q':
            self.roll -= 1
            self.roll %= 360

        if event.keysym == 'r':
            self.random_plate()
            self.redraw()
            return

        if event.keysym == 't':
            self.random_text()
            self.redraw()
            return

        if event.keysym == 'y':
            self.random_text2()
            self.redraw()
            return

        print('yaw', self.yaw, 'pitch', self.pitch, 'roll', self.roll)

        self.generate()
        self.redraw()
        

if __name__ == '__main__':
    root = tk.Tk()
    tool = GeneratorTool(root)
    tool.mainloop()