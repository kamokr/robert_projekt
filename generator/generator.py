#!/usr/bin/env python

# http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU20140001522/O/D20141522.pdf

import glob
import math
import os
import random

import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageEnhance

from generator import util
from generator.pl import prefix



FORMAT = {
    3: {
        'lddz': { 'a': 64, 'b': 12, 'c': 39, 'd': 11 }, #  6. 3L+1L, 3C
        'dzll': { 'a': 64, 'b': 11, 'c': 37, 'd': 11 }, #  9. 3L+2C, 2L
        'zldz': { 'a': 64, 'b': 12, 'c': 39, 'd': 11 }, #  7. 3L+1C, 1L, 2C
        'dzlz': { 'a': 64, 'b': 12, 'c': 39, 'd': 11 }, #  8. 3L+2C, 1L, 1C
        'zllz': { 'a': 64, 'b': 11, 'c': 37, 'd': 11 }, # 10. 3L+1C, 2L, 1C
        'lldz': { 'a': 64, 'b': 11, 'c': 37, 'd': 11 }, # 11. 3L+2L, 2C
        # 'ldzl': {}, # brak opisu w ustawie
        # 'lzll': {}, 
        'ddzll': { 'a': 55, 'b': 8, 'c': 35, 'd': 8}, # 14. 3L+3C, 2L
        'dddzl': { 'a': 55, 'b': 8, 'c': 35, 'd': 9}, # 13. 3L+4C, 1L
        'ddddz': { 'a': 55, 'b': 8, 'c': 35, 'd': 10} # 12. 3L+5C
    },
    2: {
        'ddddz': { 'a': 64, 'b': 14, 'c': 40, 'd': 14 }, #  1. 2L+5C
        'dddzl': { 'a': 64, 'b': 14, 'c': 40, 'd': 13 }, #  2. 2L+4C, 1L
        'ddzll': { 'a': 64, 'b': 14, 'c': 40, 'd': 12 }, #  3. 2L+3C, 2L
        'zlddz': { 'a': 64, 'b': 14, 'c': 40, 'd': 12 }, #  4. 2L+1C, 1L, 3C
        'zlldz': { 'a': 64, 'b': 14, 'c': 40, 'd': 12 }  #  5. 2L+1C, 2L, 2C
    }
}


DIGITS = "0123456789"
DIGITS_NO_ZERO = "123456789"
LETTERS = "ABCDEFGHIJKLMNOPRSTUVWXYZ"
UPPERCASE = LETTERS
LOWERCASE = "abcdefghijklmnoprstuvwxyz"
LOWERCASE_IDENTIFIER = "acefghjklmnprstuvwxy"

CHARS = UPPERCASE + LOWERCASE + DIGITS


POSTFIX_CHARS = {
    'd': DIGITS,
    'z': DIGITS_NO_ZERO,
    'l': LOWERCASE_IDENTIFIER
}

PLATE_WIDTH = 520
PLATE_HEIGHT = 114

FONT_SCALE_RATIO = 1.33 # scale char height by ratio to get proper rendered font height


class GeneratorConfig():
    BACKGROUNDS_DIR = '../SUN397'
    PLATE_HEIGHT = 114
    PLATE_WIDTH = 520
    TEXT_OFFSET_TOP = 17

    DATASET_WIDTH = 120
    DATASET_HEIGHT = 64

    RANDOM_SCALE = (1.0, 0.4, 0.9)
    RANDOM_DISTANCE = (1.0, 0.0, 3.0) # 2^(distance+1) * plate width: 0 for 2 * plate width, 1 for 4 * plate width ... 
    RANDOM_YAW = (1.0, -40, 40)
    RANDOM_PITCH = (1.0, -15, 30)
    RANDOM_ROLL = (1.0, -10, 10)
    RANDOM_BRIGHTNESS = (1.0, 0.5, 1.5)
    RANDOM_CONTRAST = (1.0, 0.5, 1.5)
    RANDOM_SHARPNESS = (1.0, -0.5, 1.0)
    RANDOM_NOISE = (1.0, 1.0, 10.0)

    TEXT_LENGTH_RANGE = (6, 10)
    TEXT_CHARACTERS = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

    UPSCALE = 4 # Image.transform doesn't support Lanczos filtering, so upscale for higher quality


class Noise():
    def __init__(self, image):
        self.image = image

    def enhance(self, val):
        noise = np.random.normal(0.0, val, (self.image.size[1], self.image.size[0]))
        noise = np.clip(np.array(self.image) + noise, 0, 255)
        return Image.fromarray(np.uint8(noise))


def create_random_bg_generator(bgs, w, h):

    random.shuffle(bgs)
    
    for filename in bgs:
        image = Image.open(filename).convert('L')
        print(filename)

        iw, ih = image.size

        ow = int((random.random()*0.2+0.1)*iw)
        oh = ow*h//w

        ox = random.randrange(0, iw-ow)
        oy = random.randrange(0, ih-oh)

        out = image.crop((ox, oy, ox+ow, oy+oh))
        out = out.resize((w, h), resample=Image.BICUBIC)
        yield out


def call_with_probability(p, function, default):
    if random.random() < p:
        return function()
    else:
        return default

def assign_random_value(config, default=0):
    p, vmin, vmax = config
    vmin, vmax = int(vmin*100), int(vmax*100)
    result = call_with_probability(
        p,
        lambda: random.randint(vmin, vmax)/100,
        default)
    return result


class Generator():
    def __init__(self, cfg=None):
        if cfg == None:
            self.cfg = GeneratorConfig()
        else:
            self.cfg = cfg

        self.char_im = {}
        dir_path = os.path.dirname(os.path.realpath(__file__))
        im = Image.open(os.path.join(dir_path, 'pl.png')).convert('L')
        array = np.array(im).astype(np.float32) / 255.
        self.pl = array
        self.load_font()

        pattern = os.path.join(self.cfg.BACKGROUNDS_DIR, '*/*/*.jpg')
        self.bgs = list(glob.iglob(pattern, recursive=True))
        self.random_bg_generator = create_random_bg_generator(self.bgs, self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT)


        fonts_dir = os.path.join('ttf/*/*.ttf')
        self.fonts = list(glob.iglob(fonts_dir, recursive=True))


    def random_bg(self):
        try:
            return next(self.random_bg_generator)
        except StopIteration:
            self.random_bg_generator = create_random_bg_generator(self.bgs, self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT)
            return next(self.random_bg_generator)

    def load_font(self):
        for ch in CHARS:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            im = Image.open(os.path.join(dir_path, 'font', f'{ch}.png')).convert('L')
            array = np.array(im).astype(np.float32) / 255.
            
            self.char_im[ch] = array

    def render_text(self, text=None, font=None):
        if font == None:
            font_path = random.choice(self.fonts)
        else:
            font_path = font
            
        print(font_path)
        font = ImageFont.truetype(font_path, self.cfg.DATASET_HEIGHT*2)
        

        if text == None:
            text = ''
            lmin, lmax = self.cfg.TEXT_LENGTH_RANGE
            for _ in range(random.randint(lmin, lmax)):
                text += random.choice(self.cfg.TEXT_CHARACTERS)

        total_width = 0
        max_height = 0
        for c in text:
            total_width += font.getsize(c)[0]
            height = font.getsize(c)[1]
            max_height = max(height, max_height)
        tw, th = total_width, max_height # text width and height

        # image size
        total_width += 14
        max_height += 14

        # fix ratio to match plate
        target_ratio = self.cfg.PLATE_WIDTH/self.cfg.PLATE_HEIGHT
        if total_width/max_height > target_ratio:
            h = int(total_width/target_ratio)
            w = total_width
        else:
            w = int(max_height*target_ratio)
            h = max_height

        target_size = self.cfg.PLATE_WIDTH, self.cfg.PLATE_HEIGHT
        
        mask = Image.new('L', (w, h), 0)
        color = Image.new('L', target_size, color=random.randint(0,255))

        draw = ImageDraw.Draw(mask)
        pos = (w-tw)//2, (h-th)//2
        draw.text(pos, text, 255, font=font)
        mask = mask.resize(target_size, resample=Image.LANCZOS)

        return color, mask


    def render_plate(self, pfc):
        prefix, fmt, code = pfc
        plate_shape = (self.cfg.PLATE_HEIGHT-14, self.cfg.PLATE_WIDTH-14)
        text_mask = np.zeros(plate_shape)

        # character position variables
        a = FORMAT[len(prefix)][fmt]['a']
        b = FORMAT[len(prefix)][fmt]['b']
        c = FORMAT[len(prefix)][fmt]['c']
        d = FORMAT[len(prefix)][fmt]['d']
        x = a-7
        y = self.cfg.TEXT_OFFSET_TOP-7

        pre, post = code.split(' ') # 'WF 123a' -> ['WF', '123a]

        # change prefix characters to narrow for long plate numbers
        if len(code) > 8:
            pre = pre.lower()

        for ch in pre:
            char_im = self.char_im[ch]
            char_h, char_w = char_im.shape
            text_mask[y:y + char_h, x:x + char_w] = char_im
            # print(x, '+', char_w, ch)                       
            x += char_w + b
        x -= b
        x += c

        for ch in post:
            # print(x, ch)
            char_im = self.char_im[ch]
            char_h, char_w = char_im.shape
            text_mask[y:y + char_h, x:x + char_w] = char_im
            x += char_w + d

        plate = (np.ones(plate_shape) * 1.0 * (1. - text_mask) +
                 np.ones(plate_shape) * 0.0 * text_mask)

        pl_h, pl_w = self.pl.shape
        plate[0:pl_h, 0:pl_w] = self.pl

        plate = plate * 255
        image = Image.fromarray(plate.astype(np.uint8), mode='L')
        # image.save('plate.png')
        # text_mask = text_mask * 255
        # text_mask = Image.fromarray(text_mask.astype(np.uint8), mode='L')
        # text_mask.save('mask.png')
        mask = Image.new('L', image.size, 255)

        return image, mask

    # pass keyword parameters to override generator config
    def generate(self, image, mask, yaw=None, pitch=None, roll=None, distance=0, scale=None, position=None):
        image2 = image.resize((image.size[0]*self.cfg.UPSCALE, image.size[1]*self.cfg.UPSCALE), resample=Image.BICUBIC)
        # image2_mask = Image.new('L', (image.size[0]*self.cfg.UPSCALE, image.size[1]*self.cfg.UPSCALE), color=255)
        image1_mask = mask.resize((mask.size[0]*self.cfg.UPSCALE, mask.size[1]*self.cfg.UPSCALE), resample=Image.BICUBIC)

        dw, dh = self.cfg.DATASET_WIDTH*self.cfg.UPSCALE, self.cfg.DATASET_HEIGHT*self.cfg.UPSCALE        
        iw, ih = image2.size

        # print('upscaled dataset', dw, dh)
        # print('upscaled image', iw, ih)

        iw2, ih2 = iw//2, ih//2
        corners = np.matrix([
            [-iw2, -ih2, 0, 1], [ iw2, -ih2, 0, 1],
            [ iw2,  ih2, 0, 1], [-iw2,  ih2, 0, 1]
        ])

        # rotate, translate and apply perspective
        def to_radians(angle, cfg, linear=False):
            if not linear:
                g = lambda a: math.asin(a/90)/math.pi*2*90 # util function to convert to nonlinear units
            else:
                g = lambda a: a

            if angle == None:
                p, amin, amax = cfg
                amin, amax = int(g(amin)*100), int(g(amax)*100)
                angle = call_with_probability(
                    p,
                    lambda: random.randint(amin, amax)/100,
                    0)
                # angle = assign_random_value((cfg[0], g(cfg[1]), g(cfg[2])), 0)

            if not linear:
                angle = math.radians(math.sin(math.radians(angle))*90)
            else:
                angle = math.radians(angle)
            return angle

        yaw = to_radians(yaw, self.cfg.RANDOM_YAW)
        pitch = to_radians(pitch, self.cfg.RANDOM_PITCH)
        roll = to_radians(roll, self.cfg.RANDOM_ROLL, linear=True)

        if distance == None:
            distance = assign_random_value(self.cfg.RANDOM_DISTANCE)

        print('yaw', yaw, 'pitch', pitch, 'roll', roll)
        

        corners = util.rotate(corners, (yaw, pitch, roll))
        corners = util.translate(corners, (0, 0, iw*(2**(distance+1))))
        corners = util.perspective(corners)

        # scale corners to fit upscaled dataset
        def getbox(array2d):
            x = array2d[:,0]
            y = array2d[:,1]
            return np.min(x), np.min(y), np.max(x), np.max(y)

        box = getbox(corners)

        bw, bh = box[2] - box[0], box[3] - box[1]

        cx = dw//2
        cy = dh//2

        sc = 1.0
        if scale == None:
            # p, amin, amax = self.cfg.RANDOM_SCALE
            # amin, amax = int(amin*100), int(amax*100)
            # scale = call_with_probability(
            #         sp,
            #         lambda: random.randint(amin, amax)/100,
            #         1.0)
            scale = assign_random_value(self.cfg.RANDOM_SCALE, 1.0)

        if bw/bh > dw/dh:
            sc = dw/bw*scale
        else:
            sc = dh/bh*scale
        corners = util.scale(corners, (sc, sc, 1.0))

        # print('corners scaled to fit dataset:\r\n{}'.format(corners))

        # move to center
        box = getbox(corners)
        bw, bh = box[0] + box[2], box[1] + box[3]
        cx, cy = dw//2-bw//2, dh//2-bh//2
        # print('box center: {}, {}'.format(cx, cy))
        corners = util.translate(corners, (cx, cy, 0))


        # apply random offset
        box = getbox(corners)
        if position == None:
            x1, y1, x2, y2 = box
            w, h = int(x2 - x1), int(y2 - y1)
            sx, sy = dw-w, dh-h
            offset = random.randint(0, sx)-sx//2, random.randint(0, sy)-sy//2
            print('offset: {}, box: {}x{}, offset range: {}x{}'.format(offset, w, h, sx//2, sy//2))
        else:
            offset = position

        # apply the transform
        corners2D = np.squeeze(np.asarray(corners[:,0:2]))
        coeffs = util.find_coeffs( corners2D,
            [
                (iw, ih),
                (0, ih),
                (0, 0),
                (iw, 0),
            ]
        )
        image2 = image2.transform((iw, ih), Image.PERSPECTIVE, coeffs, Image.BICUBIC)
        # image2_mask = image2_mask.transform((iw, ih), Image.PERSPECTIVE, coeffs, Image.BICUBIC)
        image1_mask = image1_mask.transform((iw, ih), Image.PERSPECTIVE, coeffs, Image.BICUBIC)

        cx = box[0]/2 + box[2]/2 + offset[0]
        cy = box[1]/2 + box[3]/2 + offset[1]
        # print('cropping', cx-dw//2, cy-dh//2, cx+dw//2, cy+dh//2)

        


        image2 = image2.crop((cx-dw//2, cy-dh//2, cx+dw//2, cy+dh//2))
        # image2_mask = image2_mask.crop((cx-dw//2, cy-dh//2, cx+dw//2, cy+dh//2))
        image1_mask = image1_mask.crop((cx-dw//2, cy-dh//2, cx+dw//2, cy+dh//2))

        # randomly postprocess, contrast and brightness is applied twice
        def apply_randomness(image, function, config):
            p = config[0]
            vmin = int(config[1]*100)
            vmax = int(config[2]*100)
            image = call_with_probability(
                p,
                lambda: function(image).enhance(
                    random.randint(vmin, vmax)/100),
                image
            )
            return image

        image2 = apply_randomness(image2, ImageEnhance.Brightness, self.cfg.RANDOM_BRIGHTNESS)
        image2 = apply_randomness(image2, ImageEnhance.Contrast, self.cfg.RANDOM_CONTRAST)


        image2 = image2.resize((self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT), resample=Image.LANCZOS)
        # image2_mask = image2_mask.resize((self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT), resample=Image.LANCZOS)
        image1_mask = image1_mask.resize((self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT), resample=Image.LANCZOS)

        #black = Image.new('L', (self.cfg.DATASET_WIDTH, self.cfg.DATASET_HEIGHT), color=0)
        #output = Image.composite(black, self.random_bg(), image1_mask)

        output = Image.composite(image2, self.random_bg(), image1_mask)

        output = apply_randomness(output, ImageEnhance.Brightness, self.cfg.RANDOM_BRIGHTNESS)
        output = apply_randomness(output, ImageEnhance.Contrast, self.cfg.RANDOM_CONTRAST)
        output = apply_randomness(output, ImageEnhance.Sharpness, self.cfg.RANDOM_SHARPNESS)

        output = apply_randomness(output, Noise, self.cfg.RANDOM_NOISE)

        return output


def code():
    p = random.choice(list(prefix.keys()))
    f, c = _gen_pl_code(p, FORMAT[len(p)])
    return p, f, c


def _gen_pl_code(p, fmt):
    f = random.choice(list(fmt.keys()))

    tmp = '{} '.format(p)
    for t in f:
        tmp += random.choice(POSTFIX_CHARS[t])
    return f, tmp

def gen_font(self, font_path):
    font_size = int(CHAR_HEIGHT * FONT_SCALE_RATIO)
    font = ImageFont.truetype(font_path, font_size)

    for c in ['L']:
        width = font.getsize(c)[0]
        height = font.getsize(c)[1]
        im = Image.new("RGBA", (width, height), (0, 0, 0))

        draw = ImageDraw.Draw(im)
        draw.text((0, 0), c, (255, 255, 255), font=font)

        # im = im.crop((6, 16, width-6, height))

        im.save('{}.png'.format(c))
        

def main():
    g = Generator()
    # g.render(('PZ', 'ddzll', 'PZ 596lt'))
    # g.render(('WSI', 'dzll', 'WSI 91jc'))
    
    # g.render(('GKA', 'ddddz', 'GKA 32410'))

    g.render(code())


if __name__ == '__main__':
    main()