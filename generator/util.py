import math

import numpy as np


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])

    A = np.matrix(matrix, dtype=np.float)
    B = np.array(pb).reshape(8)

    res = np.dot(np.linalg.inv(A.T * A) * A.T, B)
    return np.array(res).reshape(8)

def rotate(input, ypr):
    yaw, pitch, roll = ypr
    # Rotate clockwise about the Y-axis
    c, s = math.cos(yaw), math.sin(yaw)
    M = np.matrix([[  c, 0.,  s, 0.],
                   [ 0., 1., 0., 0.],
                   [ -s, 0.,  c, 0.],
                   [ 0., 0., 0., 1.]])

    # Rotate clockwise about the X-axis
    c, s = math.cos(pitch), math.sin(pitch)
    M = np.matrix([[ 1., 0., 0., 0.],
                   [ 0.,  c, -s, 0.],
                   [ 0.,  s,  c, 0.],
                   [ 0., 0., 0., 1.]]) * M

    # Rotate clockwise about the Z-axis
    c, s = math.cos(roll), math.sin(roll)
    M = np.matrix([[  c, -s, 0., 0.],
                      [  s,  c, 0., 0.],
                      [ 0., 0., 1., 0.],
                      [ 0., 0., 0., 1.]]) * M
    return input*M


def translate(input, xyz):
    x, y, z = xyz
    M = np.matrix([[1, 0, 0, x],
                      [0, 1, 0, y],
                      [0, 0, 1, z],
                      [0, 0, 0, 1]])
    return (M*input.T).T

def scale(input, xyz):
    x, y, z = xyz
    M = np.matrix([[x, 0, 0, 0],
                      [0, y, 0, 0],
                      [0, 0, z, 0],
                      [0, 0, 0, 1]])
    return (M*input.T).T


def perspective(input):
    M = np.matrix([[1,  0,  0,  0],
                      [0,  1,  0,  0],
                      [0,  0, -1, -1],
                      [0,  0,  0,  0]])
    tmp = input*M
    m00 = 1/tmp.item(0, 3)
    m11 = 1/tmp.item(1, 3)
    m22 = 1/tmp.item(2, 3)
    m33 = 1/tmp.item(3, 3)
    M = np.matrix([[m00, 0, 0, 0],
                      [0, m11, 0, 0],
                      [0, 0, m22, 0],
                      [0, 0, 0, m33]])
    return M*tmp