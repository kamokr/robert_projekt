import collections
import io
import logging
import os
import re
import time
import xml.etree.ElementTree as et
from types import SimpleNamespace

import netifaces
import requests

import taskthread

# print()

class HikvisionConfigThread(taskthread.TaskThread):
    __sleep__ = 2
    HEADERS = {'Content-Type': 'application/x-www-form-urlencoded'}
    PATH = '/ISAPI/Event/notification/httpServers/1'

    def __init__(self, camera):
        super().__init__()
        self.camera = camera
        self.time = 0

        self.ref = {
            'id': '1',
            'enabled': 'true',
            'url': '/event',
            'protocolType': 'HTTP',
            'addressingFormatType': 'ipaddress',
            'ipAddress': '',
            'portNo': '',
            'userName': None,
            'httpAuthenticationMethod': 'none',
            'uploadPicture': 'false',
            'pictureType': 'big',
        }

    def __str__(self):
        return f'ANPR {self.__class__.__name__}()'

    def namespace(self, element):
        m = re.match('\{.*\}', element.tag)
        return m.group(0) if m else ''

    def build_xml(self):
        xml = '<HttpServer version="2.0">'
        for key, value in self.ref.items():
            xml += '<{}>{}</{}>'.format(key, value, key)
        xml += '</HttpServer>'
        return xml

    def check(self):
        url = 'http://{}:{}{}'.format(self.camera.ipv4, self.camera.port, self.PATH)
        auth = (self.camera.username, self.camera.password)

        response = requests.get(url, auth=auth, timeout=2)

        if response.status_code == 401:
            return True
        elif response.status_code != 200:
            return True
        else:
            self.ref['ipAddress'] = netifaces.ifaddresses('eth0')[netifaces.AF_INET][0]['addr']
            self.ref['portNo'] = 12080

            xml = response.text

            logging.debug(xml)
            root = et.fromstring(xml)

            current = {}
            for key in self.ref.keys():
                current[key] = root.find('{xmlns}{tag}'.format(xmlns=self.namespace(root), tag=key)).text

            for key, value in self.ref.items():
                if str(value) != str(current[key]):
                    logging.warning('DEVICES {} {}: {} != {}'.format(self, key, current[key], value))
                    return False

            return True

    def task(self):
        if self.time == 5:
            if self._terminate:
                return
            try:
                if not self.check():
                    url = 'http://{}:{}{}'.format(self.camera.ipv4, self.camera.port, self.PATH)
                    auth = (self.camera.username, self.camera.password)
                    requests.put(url, auth=auth, headers=self.HEADERS, data=self.build_xml(), timeout=5)
            except OSError as ex:
                # logging.exception(ex)
                pass
            except Exception as ex:
                logging.exception(ex)
        self.time += 5
        self.time %= 60


class Camera:
    ipv4 = None
    port = 80
    username = None
    password = None



if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    c = Camera()
    c.ipv4 = '192.168.1.110'
    c.username = 'admin'
    c.password = '12345678a'

    thread = HikvisionConfigThread(c)

    thread.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    
    thread.stop()