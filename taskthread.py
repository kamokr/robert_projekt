import logging
import threading
import time


class TaskThread(threading.Thread):
    __sleep__ = 0
    
    def __init__(self):
        super().__init__()
        self._counter = 0
        self._terminate = False
    
    def task(self):
        pass
    
    def run(self):
        logging.debug('{} started'.format(str(self)))
        while not self._terminate:
            if self.__sleep__ == 0 or self._counter == self.__sleep__:
                try:
                    self.task()
                except Exception as ex:
                    logging.exception(ex)
                self._counter = 0
                
            if self.__sleep__ != 0:
                time.sleep(1)
                self._counter += 1

        logging.debug('{} stopped'.format(str(self)))
        
    def stop(self):
        logging.debug('{} stopping'.format(str(self)))
        self._terminate = True